import { useCallback } from 'react';
import { useDispatch } from 'react-redux';

export function useDispatchCallback() {
  const dispatch = useDispatch();
  const dispatchCallback = useCallback(dispatch, []);

  return dispatchCallback;
}
