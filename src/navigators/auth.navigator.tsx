import React from 'react';
import {
  createStackNavigator,
  StackNavigationOptions,
} from '@react-navigation/stack';

import { LoginPage } from '../pages/login/login.component';

const Stack = createStackNavigator();

const defaultOptions: StackNavigationOptions = {
  headerShown: false,
};

export const AuthNavigator = () => {
  return (
    <Stack.Navigator headerMode="screen" screenOptions={defaultOptions}>
      <Stack.Screen name="Login" component={LoginPage} />
    </Stack.Navigator>
  );
};
