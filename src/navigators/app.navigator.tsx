import React from 'react';
import {
  createStackNavigator,
  StackNavigationOptions,
} from '@react-navigation/stack';

import { MainPage } from '../pages/main/main.component';
import { SettingsPage } from '../pages/settings/settings.component';

const Stack = createStackNavigator();

const defaultOptions: StackNavigationOptions = {
  headerShown: false,
};

export const AppNavigator = () => {
  return (
    <Stack.Navigator headerMode="screen" screenOptions={defaultOptions}>
      <Stack.Screen name="Main" component={MainPage} />
      <Stack.Screen name="Settings" component={SettingsPage} />
    </Stack.Navigator>
  );
};
