export type SessionState = {
  readonly isAuthenticated: boolean;
};

export enum SessionActionTypes {
  SIGN_IN = '@session/SIGN_IN',
  SIGN_OUT = '@session/SIGN_OUT',
}

export type SignInAction = {
  type: SessionActionTypes.SIGN_IN;
};

export type SignOutAction = {
  type: SessionActionTypes.SIGN_OUT;
};

export type SessionAction = SignInAction | SignOutAction;
