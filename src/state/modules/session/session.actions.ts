import { SessionAction, SessionActionTypes } from './session.types';

export const signIn = (): SessionAction => ({
  type: SessionActionTypes.SIGN_IN,
});

export const signOut = (): SessionAction => ({
  type: SessionActionTypes.SIGN_OUT,
});
