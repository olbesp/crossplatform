import {
  SessionState,
  SessionActionTypes,
  SessionAction,
} from './session.types';

const initialState: SessionState = {
  isAuthenticated: false,
};

export const sessionReducer = (
  state: SessionState = initialState,
  action: SessionAction
): SessionState => {
  switch (action.type) {
    case SessionActionTypes.SIGN_IN:
      return { ...state, isAuthenticated: true };

    case SessionActionTypes.SIGN_OUT:
      return { ...state, isAuthenticated: false };

    default:
      return state;
  }
};
