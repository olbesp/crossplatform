import { createSelector } from 'reselect';

import { State } from '..';

export const getSessionState = (state: State) => state.session;

export const selectIsAuthenticated = createSelector(
  [getSessionState],
  (sessionState) => sessionState.isAuthenticated
);
