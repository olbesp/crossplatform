import { combineReducers, Reducer, AnyAction } from 'redux';

import { sessionReducer, SessionActionTypes } from './session';

const combinedReducer = combineReducers({
  session: sessionReducer,
});

// Clear app state when signout action fires
export const rootReducer: Reducer = (state, action: AnyAction) => {
  if (action.type === SessionActionTypes.SIGN_OUT) {
    state = undefined;
  }

  return combinedReducer(state, action);
};

export type State = ReturnType<typeof rootReducer>;
