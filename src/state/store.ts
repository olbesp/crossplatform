import Redux, { createStore, applyMiddleware } from 'redux';
import logger from './middlewares/logger.middleware';

import { rootReducer } from './modules';

const middlewares: Redux.Middleware[] = [];

if (process.env.NODE_ENV === 'development' || __DEV__) {
  middlewares.push(logger);
}

export const store = createStore(rootReducer, applyMiddleware(...middlewares));
