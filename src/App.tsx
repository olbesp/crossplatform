import React from 'react';
import { Provider as ReduxStateProvider } from 'react-redux';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import { RootNavigator } from './navigators/root.navigator';

import { store } from './state/store';

export default function App() {
  return (
    <SafeAreaProvider>
      <ReduxStateProvider store={store}>
        <RootNavigator />
      </ReduxStateProvider>
    </SafeAreaProvider>
  );
}
