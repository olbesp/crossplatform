import React from 'react';

import {
  TextInput,
  View,
  TextInputProps,
  StyleProp,
  ViewStyle,
  Text,
} from 'react-native';

import { styles } from './input.styles';

interface Props extends TextInputProps {
  containerStyle?: StyleProp<ViewStyle>;
  label: string;
}

type InputBorderColor = 'pink' | 'green';

export const Input = ({ label, containerStyle, ...otherProps }: Props) => {
  const [borderColor, setBorderColor] = React.useState<InputBorderColor>(
    'green'
  );

  const handleFocus = React.useCallback(() => {
    setBorderColor('pink');
  }, []);

  const handleBlur = React.useCallback(() => {
    setBorderColor('green');
  }, []);

  return (
    <View style={[containerStyle, styles.container]}>
      <Text style={styles.label}>{label}</Text>
      <TextInput
        {...otherProps}
        style={[styles.input, { borderColor }]}
        onFocus={handleFocus}
        onBlur={handleBlur}
      />
    </View>
  );
};
