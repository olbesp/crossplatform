import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    height: 60,
    width: '100%',
  },
  label: {
    fontSize: 14,
    color: '#7e7e7e',
  },
  input: {
    borderWidth: 1,
    borderRadius: 4,
    height: 40,
  },
});
