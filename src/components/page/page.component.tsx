import React from 'react';

import { SafeAreaView } from 'react-native-safe-area-context';
import { ScrollView } from 'react-native';

import { styles } from './page.styles';

type Props = {
  children: JSX.Element | JSX.Element[];
};

export const Page = ({ children }: Props) => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView contentContainerStyle={styles.contentContainer}>
        {children}
      </ScrollView>
    </SafeAreaView>
  );
};
