import React from 'react';

import { Text } from '../text/text.component';

import styles from './page.module.scss';

type Props = {
  children: JSX.Element | JSX.Element[];
};

export const Page = ({ children }: Props) => {
  return (
    <div className={styles.page}>
      <Text style={{ color: 'black' }}>BLABLABLA</Text>
      {children}
    </div>
  );
};
