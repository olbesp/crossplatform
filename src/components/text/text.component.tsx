import React from 'react';

import { Text as RNText, TextProps } from 'react-native';

import { styles } from './text.styles';

interface Props extends TextProps {
  children?: string | number;
  isHuge?: boolean;
}

export const Text = ({ children, isHuge, ...otherProps }: Props) => {
  return (
    <RNText
      {...otherProps}
      style={[
        styles.text,
        {
          fontSize: isHuge ? 56 : 15,
        },
        otherProps.style,
      ]}
    >
      {`${children}` || ''}
    </RNText>
  );
};
