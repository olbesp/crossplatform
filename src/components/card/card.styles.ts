import { StyleSheet } from 'react-native';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../../utils/constants';

export const styles = StyleSheet.create({
  container: {
    width: Math.round(SCREEN_WIDTH * 0.8),
    height: Math.round(SCREEN_HEIGHT * 0.8),
    borderRadius: Math.round(SCREEN_WIDTH * 0.02),
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    maxWidth: 600,
    marginVertical: Math.round(SCREEN_HEIGHT * 0.1),
    marginHorizontal: 'auto',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.2,
    shadowRadius: 8,
    elevation: 3,
    backgroundColor: 'white',
  },
});
