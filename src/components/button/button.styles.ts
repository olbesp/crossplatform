import { StyleSheet } from 'react-native';
import { SCREEN_WIDTH } from '../../utils/constants';

export const styles = StyleSheet.create({
  container: {
    width: SCREEN_WIDTH * 0.75,
    height: SCREEN_WIDTH * 0.05,
  },
});
