import React from 'react';

import { Button as RNButton, ButtonProps } from 'react-native';

interface Props extends ButtonProps {
  isBrown?: boolean;
}

export const Button = ({ isBrown, ...otherProps }: Props) => {
  return <RNButton {...otherProps} color={isBrown ? 'brown' : 'red'} />;
};
