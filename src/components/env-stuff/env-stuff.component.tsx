import React from 'react';

import { Text } from '../text/text.component';

const env = require('../../config/env.json');

export const EnvStuff = () => (
  <Text style={{ color: 'green' }}>{env.SOME_VAR}</Text>
);
