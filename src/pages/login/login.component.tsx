import React from 'react';

import { View } from 'react-native';

import { Button } from '../../components/button/button.component';
import { Input } from '../../components/input/input.component';
import { useDispatchCallback } from '../../hooks';
import { signIn } from '../../state/modules/session';
import { Page } from '../../components/page/page.component';
import { Card } from '../../components/card/card.component';

export const LoginPage = () => {
  const [value, setValue] = React.useState<string>('');

  const dispatch = useDispatchCallback();

  const handleSubmit = React.useCallback(() => {
    dispatch(signIn());
  }, [dispatch]);

  return (
    <Page>
      <Card>
        <View style={{ width: '60%' }}>
          <Input
            label="Enter what you want"
            value={value}
            onChangeText={(newValue: string) => setValue(newValue)}
            containerStyle={{ marginVertical: 30 }}
          />
        </View>

        <Button onPress={handleSubmit} title="Submit" />
      </Card>
    </Page>
  );
};
