import React from 'react';

import { Button } from '../../components/button/button.component';
import { useDispatchCallback } from '../../hooks';
import { signOut } from '../../state/modules/session';
import { Page } from '../../components/page/page.component';
import { EnvStuff } from '../../components/env-stuff/env-stuff.component';
import { Card } from '../../components/card/card.component';
import { Platform } from 'react-native';
import { Link } from '@react-navigation/native';

export const MainPage = ({ navigation }: any) => {
  const dispatch = useDispatchCallback();

  const handleLogout = React.useCallback(() => {
    dispatch(signOut());
  }, [dispatch]);

  return (
    <Page>
      <Card>
        {Platform.OS === 'web' ? (
          <Link to="/settings">SETTING</Link>
        ) : (
          <Button
            title="Go back to Settings page"
            onPress={() => navigation.navigate('Settings')}
          />
        )}
        <Button title="LOGOUT" onPress={handleLogout} />
        <EnvStuff />
      </Card>
    </Page>
  );
};
