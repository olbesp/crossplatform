import React from 'react';

import { Button } from '../../components/button/button.component';
import { Page } from '../../components/page/page.component';
import { Card } from '../../components/card/card.component';

export const SettingsPage = ({ navigation }: any) => {
  return (
    <Page>
      <Card>
        <Button
          title="Go back to Main page"
          onPress={() => navigation.navigate('Main')}
        />
      </Card>
    </Page>
  );
};
